﻿using Common.Interfaces;
using Common.Interfaces.Messages;
using Common.Workers;
using System;

namespace Workers
{
    public class LongStringReaderWorker : QueuedWorkerBase, ILongStringReaderWorker
    {

        private String content;

        public override void InitializeWorker()
        {
        }

        public override bool Process(object item)
        {
            if (item != null)
            {
                this.content = MessageBase<String>.GetBody((MessageBase<String>)item);
                //Console.WriteLine($"{this.workerQueue.Count} | {((MessageBase<String>)item).MessageId} | {(DateTime.UtcNow - ((MessageBase<String>)item).CreateTime)} | {DateTime.UtcNow.ToString("o")} | {content.ToString().Substring(0,10)}");
            }
            return true;
        }

        public override Type[] RegisterMessageType()
        {
            return new Type[] {
                typeof(MessageBase<String>)
                };
        }

    }
}
