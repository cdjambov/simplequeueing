﻿using Common.Interfaces;
using Common.Enums;
using Common.Interfaces.Messages;
using System;
using System.Threading;
using Common.Workers;
using System.IO;

namespace Workers
{
    public class LongStringWriterWorker : QueuedWorkerBase, ILongStringWriterWorker
    {
        private string content;

        public string Content
        {
            get
            {
                if (string.IsNullOrEmpty(content))
                    content = File.ReadAllText(@"1MBtxtfile.txt");
                return content;
            }
        }
        public override void InitializeWorker()
        {
            postString();
        }

        public override bool Process(object Item)
        {
            //We will do nothing here as we will only post mesages 
            return true;
        }

        public override Type[] RegisterMessageType()
        {
            return new Type[] { };
            // and we will not register for any types 
            //builder.RegisterInstance(this).As(typeof(MessageBase<String>));
        }

        private void postString()
        {
            //this.QueueMessage(MessageBase<DownloadWebPage>.CreateMessage(new DownloadWebPage {Url = "www.azlyrics.com" }));
            this.content = File.ReadAllText(@"1MBtxtfile.txt");
            int i = 10000;
            while (this.Workerstate != WorkerStateEnum.Stopping && this.Workerstate != WorkerStateEnum.Stopped && this.Workerstate != WorkerStateEnum.Starting)
            {
                //Thread.Sleep(0);
                this.QueueMessage(MessageBase<String>.CreateMessage(content));
                i--;
                if (i == 0) break;
            }
        }

    }
}
