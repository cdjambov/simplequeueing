﻿using Common.Interfaces;
using Common.Enums;
using Common.Interfaces.Messages;
using System;
using System.Threading;
using Common.Workers;

namespace Workers
{
    public class TimeWriterWorker : QueuedWorkerBase, ITimeWriter
    {


        public override void InitializeWorker()
        {
            postTime();
        }

        public override bool Process(object Item)
        {
            //We will do nothing here as we will only post mesages 
            return true;
        }

        public override Type[] RegisterMessageType()
        {
            return new Type[] { };
            // and we will not register for any types 
            //builder.RegisterInstance(this).As(typeof(MessageBase<DateTime>));
        }
        
        private void postTime() 
        {
            //this.QueueMessage(MessageBase<DownloadWebPage>.CreateMessage(new DownloadWebPage {Url = "www.azlyrics.com" }));

            while (this.Workerstate != WorkerStateEnum.Stopping && this.Workerstate != WorkerStateEnum.Stopped)
            {
                Thread.Sleep(50);
                this.QueueMessage(MessageBase<DateTime>.CreateMessage(DateTime.Now));
            }
        }

    }
}
