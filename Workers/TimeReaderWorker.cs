﻿using Common.Interfaces;
using Common.Interfaces.Messages;
using Common.Workers;
using System;

namespace Workers
{
    public class TimeReaderWorker : QueuedWorkerBase, ITimeReader
    {

        private DateTime message;

        public override void InitializeWorker()
        {
        }

        public override bool Process(object item)
        {
            if (item != null)
            {
                this.message = MessageBase<DateTime>.GetBody((MessageBase<DateTime>)item);
                //Console.WriteLine($"{message} | {(DateTime.Now - message).ToString()}");
            }
            return true;
        }

        public override Type[] RegisterMessageType()
        {
            return new Type[] {
                typeof(MessageBase<DateTime>)
                };
        }

    }
}
