﻿using Common.Enums;
using System;

namespace Common.Interfaces
{
    public interface IQueuedWorker
    {
        WorkerStateEnum Workerstate { get; }
        void AddItem(object item);
        void Start();
        void Stop();
        Type[] RegisterMessageType();
        void DoWork();
    }
}
