﻿using Common.Enums;
using Common.Interfaces;
using Common.Interfaces.Messages;
using log4net;
using System;
using System.Collections;
using System.Threading;

namespace Common.Workers
{
    public abstract class QueuedWorkerBase : IQueuedWorker
    {
        protected static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Thread workerThread;
        private ManualResetEvent workerResetEvent = new ManualResetEvent(true);
        protected Queue workerQueue;
        private object workerQueueLock = new object();
        private bool shouldStop;

        public string WorkerThreadName { get; set; }
        
        public double TotalMessagesProcessed { get; set; }
        public double TotalMessagesEnqueued
        {
            get { return workerQueue.Count; }
        }

        public double PeekLocked { get; private set; }

        public double AddLocked { get; private set; }
        
        public event EventHandler<MessageEventArgs> PostMessage;

        private WorkerStateEnum workerState;
        public WorkerStateEnum Workerstate
        {
            get { return workerState; }
        }

        public void InitializeThread()
        {
            workerThread = new Thread(new ThreadStart(DoWork));
            workerQueue = new Queue();
        }


        public QueuedWorkerBase()
        {
            InitializeThread();
            TotalMessagesProcessed = 0;

            PeekLocked = 0;
            
            AddLocked = 0;
            
        }

        /// <summary>
        /// CTor
        /// </summary>
        /// <param name="threadName">Name for the thread so that is it easily destinguishable in logs</param>
        public QueuedWorkerBase(string threadName)
        {
            InitializeThread();
            workerThread.Name = threadName;
        }

        /// <summary>
        /// Add and Item to the queue for processing. Starts the thread if it has been suspended due to no work.
        /// </summary>
        /// <param name="item">The item to add</param>
        public virtual void AddItem(object item)
        {
            lock (workerQueueLock)
            {
                AddLocked++;
                workerQueue.Enqueue(item);
                AddLocked--;
            }
            
            workerState = WorkerStateEnum.Processing;
            workerResetEvent.Set();

        }

        /// <summary>
        /// Post a message to manager queue
        /// </summary>
        /// <param name="message"></param>
        public void QueueMessage(IMessage message)
        {
            this.PostMessage?.Invoke(this, new MessageEventArgs(message));
        }


        /// <summary>
        /// Kickstart the thread. Use onlym when initializing.
        /// </summary>
        public void Start()
        {
            workerState = WorkerStateEnum.Starting;
            shouldStop = false;
            workerThread.Start();
            InitializeWorker();
            Log.InfoFormat("Thread {0} has been started.", WorkerThreadName);
            workerState = WorkerStateEnum.Waiting;

        }

        /// <summary>
        /// Used to send stop signal to the thread.
        /// </summary>
        public void Stop()
        {
            shouldStop = true;
            workerResetEvent.Set(); //let the thread escape gracefully
            workerState = WorkerStateEnum.Stopping;
            //log stop the thread, thread name
        }

        public abstract bool Process(object item);

        public abstract Type[] RegisterMessageType();
        /// <summary>
        /// Override to start you worker in uniqie way.
        /// </summary>
        public abstract void InitializeWorker();

        /// <summary>
        /// The actual method the thread is executing.
        /// </summary>
        public void DoWork()
        {
            while (!shouldStop)
            {
                while (workerQueue.Count > 0)
                {
                    //ToDo : Use Microsoft ExceptionHandling Block
                    object nextQueueItem = null;

                    lock (workerQueueLock)
                    {
                        PeekLocked++;
                        try
                        {
                            nextQueueItem = workerQueue.Peek();
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("kaboom");
                        }
                        PeekLocked--;
                    }

                    if (nextQueueItem != null)
                        if (Process(nextQueueItem))
                        {
                            TotalMessagesProcessed++;
                            workerQueue.Dequeue();
                        }
                        else
                        {
                            Console.WriteLine("MessageProcessing failed");
                        }
                }

                workerResetEvent.WaitOne();
                workerResetEvent.Reset();
                workerState = WorkerStateEnum.Waiting;
//#if DEBUG
//                Console.WriteLine($"{this.WorkerThreadName} {workerState} | {TotalMessagesProcessed}/{TotalMessagesEnqueued} | Add {AddLocked} | Peek {PeekLocked} | Queue Lenght {workerQueue.Count}");

//#endif
            }
            workerState = WorkerStateEnum.Stopped;
        }

        //public bool PurgeMessage(IMessage message)
        //{
        //    if (this.workerQueue.Contains(message.MessageId))
        //    {
        //        workerQueue.
        //    }
        //}
    }
}
