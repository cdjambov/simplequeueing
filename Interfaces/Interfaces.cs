﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces
{

    public interface IPageParse
    {
    }

    public interface IPageDownloader
    {
    }

    public interface ITimeReader
    {
    }

    public interface ITimeWriter
    {
    }

    public interface ITickReader
    {
    }

    public interface ITickPublisher
    {
    }

    public interface IPersistentWorker
    {
    }

    public interface ICqgQuotesReader : ITickReader
    {
    }

    public interface ICqgQuotesPublisher : ITickPublisher
    {
    }

    public interface ILongStringReaderWorker
    {

    }

    public interface ILongStringWriterWorker
    {

    }
}
