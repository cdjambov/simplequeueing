﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.Messages
{
    public class DownloadWebPage
    {
        public string Url { get; set; }
        public string PageContent { get; set; }
    }

}
