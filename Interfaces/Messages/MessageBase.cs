﻿using Common.Interfaces.Messages;
using System;
using System.Runtime.Serialization;

namespace Common.Interfaces.Messages
{
    [DataContract]
    public class MessageBase<T> : IMessage
    {
        [DataMember]
        public Guid MessageId { get; set; }

        private readonly DateTime createTime;

        [DataMember]
        public DateTime CreateTime => createTime;

        [DataMember]
        public T Body { get; set; }

        public MessageBase()
        {
            MessageId = Guid.NewGuid();
            Body = default(T);
            createTime = DateTime.UtcNow;
        }

        public static MessageBase<T> CreateMessage(Guid messageId, T body)
        {
            return new MessageBase<T> { MessageId = messageId, Body = body };
        }

        public static MessageBase<T> CreateMessage(T body)
        {
            return new MessageBase<T> { MessageId = Guid.NewGuid(), Body = body };
        }

        public static T GetBody(MessageBase<T> message)
        {
            return (T)message.Body;
        }
    }


    public delegate void EnqueueMessageDelegate<MessageEventArgs>(object sender, EventArgs e);


    public class MessageEventArgs : EventArgs
    {
        private IMessage message;
        public IMessage Message { get { return message; } }

        public MessageEventArgs(IMessage m)
        {
            message = m;
        }
    }

}