﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Interfaces.Messages
{
    public interface IMessage
    {
        Guid MessageId { get; set; }
    }
}
