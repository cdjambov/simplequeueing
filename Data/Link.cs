﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class Link
    {
        public string Url { get; set; }
        public string PageContent { get; set; }
        public DateTime LastChecked { get; set; }
        public TimeSpan MetaRefresh { get; set; }
        public string Charset { get; set; }
        public string Keywords { get; set; }
        public string Title { get; set; }
    }
}
