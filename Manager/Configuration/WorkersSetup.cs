﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manage.Configuration
{
    public class WorkersSetupSection : ConfigurationSection
    {
        // Declare the Urls collection property using the 
        // ConfigurationCollectionAttribute. 
        // This allows to build a nested section that contains 
        // a collection of elements.
        [ConfigurationProperty("WorkersSetup", IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(WorkersSetupCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public WorkersSetupCollection WorkersSetup
        {
            get
            {
                WorkersSetupCollection WorkersSetup =
                    (WorkersSetupCollection)base["WorkersSetup"];
                return WorkersSetup;
            }
        }

    }
    
    public class WorkersSetupCollection : ConfigurationElementCollection
    {
        public WorkersSetupCollection()
        {
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new WorkerSetupElement();
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((WorkerSetupElement)element).InterfaceName;
        }

        public WorkerSetupElement this[int index]
        {
            get
            {
                return (WorkerSetupElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        new public WorkerSetupElement this[string Name]
        {
            get
            {
                return (WorkerSetupElement)BaseGet(Name);
            }
        }

        public int IndexOf(WorkerSetupElement workerSetup)
        {
            return BaseIndexOf(workerSetup);
        }

        public void Add(WorkerSetupElement workerSetup)
        {
            BaseAdd(workerSetup);
        }

        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        public void Remove(WorkerSetupElement workerSetup)
        {
            if (BaseIndexOf(workerSetup) >= 0)
                BaseRemove(workerSetup.InterfaceName);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }
    }

    public class WorkerSetupElement : ConfigurationElement
    {
        public WorkerSetupElement()
        {
        }

        public WorkerSetupElement( string interfaceName, string className, int instanceCount)
        {
            //this.Name = name;
            this.InstanceCount = instanceCount;
            this.InterfaceName = interfaceName;
            this.ClassName = className;
        }

        [ConfigurationProperty("instanceCount", IsRequired = true)]
        public int InstanceCount
        {
            get
            {
                return (Int32)this["instanceCount"];
            }
            set
            {
                this["instanceCount"] = value;
            }
        }


        [ConfigurationProperty("interfaceName", IsRequired = true)]
        public String InterfaceName
        {
            get
            {
                return (String)this["interfaceName"];
            }
            set
            {
                this["interfaceName"] = value;
            }
        }

        [ConfigurationProperty("className", IsRequired = true)]
        public String ClassName
        {
            get
            {
                return (String)this["className"];
            }
            set
            {
                this["className"] = value;
            }
        }
    }
}
