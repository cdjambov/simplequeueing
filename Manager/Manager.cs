﻿using Autofac;
using log4net;
using Manage.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac.Core;
using Common.Interfaces.Messages;
using Common.Interfaces;
using Common.Workers;
using System.Diagnostics;

namespace Manage
{
    public class Manager : IManager
    {
        protected static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Queue<IMessage> messages;

        public ContainerBuilder builder;
        public Autofac.IContainer Container { get; set; }
        public Dictionary<Type, Type> MessageSubscriptions; 
        public Manager()
        {
            builder = new ContainerBuilder();
            MessageSubscriptions = new Dictionary<Type, Type>();
        }

        public void Start()
        {
#if DEBUG
            Debug.WriteLine($"Manager.Start {DateTime.UtcNow.ToString("o")}");
#endif
            messages = new Queue<IMessage>();
            LoadWorkers(); 

        }

        public void LoadWorkers()
        {
            
            //1. Load workers defined in the config. 
            var configuration = ConfigurationManager.GetSection("workersSetupSection") as WorkersSetupSection;
            
            List<IQueuedWorker> workers = new List<IQueuedWorker>();

            foreach (WorkerSetupElement el in configuration.WorkersSetup)
            {
                var instance = (QueuedWorkerBase)Activator.CreateInstance(Type.GetType(el.ClassName));
                instance.WorkerThreadName = el.ClassName;
                instance.PostMessage += EnqueueMessage;
                Type intface = instance.GetType().GetInterfaces().Where(x=>x.Name == el.InterfaceName).First();
                builder.RegisterInstance(instance).As(intface);
                
                foreach (Type t in (Type[])instance.GetType().InvokeMember("RegisterMessageType", BindingFlags.InvokeMethod, null, instance, null))
                {
                    MessageSubscriptions.Add(t, intface);
                }

                workers.Add(instance);
            }

            Container = builder.Build();
            Parallel.ForEach(workers, x => x.Start());

            //2. Scan For workers in the local directory.
            //3. Scan for workers elsewhere.
        }


        public void EnqueueMessage(object sender, MessageEventArgs e)
        {

            Type t1 = MessageSubscriptions[e.Message.GetType()];
            
            QueuedWorkerBase queuedWorker = (QueuedWorkerBase)this.Container.Resolve(t1);

            queuedWorker.AddItem(e.Message);

#if DEBUG
            Console.SetCursorPosition(0,0);
            foreach (var workerType in MessageSubscriptions.Values.OrderBy(t=>t.Name))
            {
                var worker = (QueuedWorkerBase)this.Container.Resolve(workerType);
                Console.WriteLine($"{worker.WorkerThreadName} {worker.Workerstate} | {worker.TotalMessagesProcessed}/{worker.TotalMessagesEnqueued} | Add {worker.AddLocked} | Peek {worker.PeekLocked}                                 ");
            }
#endif

        }

        public void ScanForWorkers()
        {
            throw new NotImplementedException();
        }

        public QueuedWorkerBase GetWorker(Type type)
        {
            throw new NotImplementedException();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }
    }
}
