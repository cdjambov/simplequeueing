﻿using Common.Workers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workers;

namespace Manage
{
    public interface IManager
    {
        void Start();
        void LoadWorkers();
        void ScanForWorkers();
        QueuedWorkerBase GetWorker(Type type);
        void Stop();

    }
}
